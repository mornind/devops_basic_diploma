variable "aws_hosted_zone" {
    type        = string
    description = "ID of route_53 hosted zone"
    default     = "Z087923213QPC3E1ZT5N6"
}