import subprocess
import json
import os

# Запускаем бэкенд:
os.system('cd s3 && terraform init && terraform apply --auto-approve')
# Запускаем основной конфиг:
os.system('cd terraform_config && terraform init && terraform apply --auto-approve')
# Деплоим докер:
command = "aws ec2 describe-instances --region us-east-1 --query 'Reservations[*].Instances[*].PublicIpAddress'"
host = subprocess.check_output(command, shell=True).decode().replace('[\n', '').replace(' ', '').replace('"', '').replace(']', '').replace('\n', '')
with open(os.path.join('ansible', 'hosts'), 'w') as hostfile:
    hostfile.write(f'[app]\napp_1 ansible_host={host} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa')
os.system('cd ansible && ansible-playbook docker_ubuntu.yml')