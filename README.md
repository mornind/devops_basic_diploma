# infra
Infrastructure of diploma project

* Не забыв активировать нужный ключ в конфиге терраформа, поднять инфру:
```
terraform init
terraform apply -auto-approve
```
* Узнать Ip полученного инстанса (или инстансов, если мы скейлим их):
```
aws ec2 describe-instances --region us-east-1 --query 'Reservations[*].Instances[*].PublicIpAddress'
```
* Этот адрес или эти адреса внести в hosts
* Запустить плейбук:
```
ansible-playbook docker_ubuntu.yml
```
* Готово, можно переходить к деплою сервиса
